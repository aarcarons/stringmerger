using System.Collections.Generic;
using System.Linq;
using ConsoleApp4;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private static string s1 = "my&friend&Paul has heavy hats! &";
        private static string s2 = "my friend John has many many friends &";

        private static uint[] map1 = MergerEngine.BuildCharMap(s1);
        private static uint[] map2 = MergerEngine.BuildCharMap(s2);

        [TestMethod]
        public void CountOnlyLowerCase()
        {
            var str = "aaAA";
            var charMap = MergerEngine.BuildCharMap(str);
            // "a" slot with 2 occurrences
            Assert.AreEqual(charMap.Take(1).First(), (uint)2);
            // The rest with 0 occurrences
            Assert.IsTrue(charMap.Skip(1).All(occurrences => occurrences == 0));
        }

        [TestMethod]
        public void LowerCaseAlphabetHas26Slots()
        {
            var charMap = MergerEngine.BuildCharMap("");
            Assert.IsTrue(charMap.Length == 25);
        }

        [TestMethod]
        public void EmptyOrNullStringsAreHandledGracefully()
        {
            MergerEngine.BuildCharMap(null);
            MergerEngine.BuildCharMap(string.Empty);
            // Expect no exceptions
        }

        [TestMethod]
        public void ZeroOrOneOccurrencesDontProduceAResult()
        {
            var dict = new Dictionary<int, uint[]>
            {
                {1, MergerEngine.BuildCharMap("abc")},
                {2, MergerEngine.BuildCharMap("abc")}
            };
            var mergedMaps = MergerEngine.MergeMaps(dict);
            Assert.IsTrue(!mergedMaps.Any());
        }

        [TestMethod]
        public void ExpectedResults()
        {
            var dict = new Dictionary<int, uint[]>
            {
                {1, map1},
                {2, map2}
            };
            var expectedResults = new List<MergeResult>
            {
                new MergeResult('n') {ContainedInString = {2}, MaxOccurrences = 5},
                new MergeResult('a') {ContainedInString = {1}, MaxOccurrences = 4},
                new MergeResult('h') {ContainedInString = {1}, MaxOccurrences = 3},
                new MergeResult('m') {ContainedInString = {2}, MaxOccurrences = 3},
                new MergeResult('y') {ContainedInString = {2}, MaxOccurrences = 3},
                new MergeResult('d') {ContainedInString = {2}, MaxOccurrences = 2},
                new MergeResult('f') {ContainedInString = {2}, MaxOccurrences = 2},
                new MergeResult('i') {ContainedInString = {2}, MaxOccurrences = 2},
                new MergeResult('r') {ContainedInString = {2}, MaxOccurrences = 2},
                new MergeResult('e') {ContainedInString = {1, 2}, MaxOccurrences = 2},
                new MergeResult('s') {ContainedInString = {1, 2}, MaxOccurrences = 2}
            };
            var mergeResults = MergerEngine.MergeMaps(dict);
            Assert.IsTrue(mergeResults.All(expectedResults.Contains));
            Assert.IsTrue(mergeResults.Count() == expectedResults.Count);
        }

        // TODO: Validate sorting of output string
    }
}
