﻿using System.Collections.Generic;

namespace ConsoleApp4
{
    public static class MergerEngine
    {
        public const int CharMapSize = 'z' - 'a';

        public static uint[] EmptyCharMap = new uint[CharMapSize];

        public static uint[] BuildCharMap(string str)
        {
            if (string.IsNullOrEmpty(str)) return EmptyCharMap;

            var charMap = new uint[CharMapSize];
            foreach (var c in str)
            {
                if (c >= 'a' && c <= 'z')
                    charMap[EncodeAsDecimal(c)]++;
            }
            return charMap;
        }

        static int EncodeAsDecimal(char character) => character - 'a';

        static char DecodeFromDecimal(int decimalValue) => (char)('a' + decimalValue);

        public static IEnumerable<MergeResult> MergeMaps(Dictionary<int, uint[]> charMapCollection)
        {
            for (int i = 0; i < CharMapSize; i++)
            {
                MergeResult mergeResult = null;
                foreach (var kv in charMapCollection)
                {
                    var occurrences = kv.Value[i];
                    if (occurrences > 1)
                    {
                        if (mergeResult == null) mergeResult = new MergeResult(DecodeFromDecimal(i));
                        if (mergeResult.MaxOccurrences < occurrences)
                        {
                            mergeResult.MaxOccurrences = occurrences;
                            mergeResult.ContainedInString.Clear();
                            mergeResult.ContainedInString.Add(kv.Key);
                        }
                        else if (mergeResult.MaxOccurrences == occurrences)
                        {
                            mergeResult.ContainedInString.Add(kv.Key);
                        }
                    }
                }
                if (mergeResult != null) yield return mergeResult;
            }
        }
    }
}
