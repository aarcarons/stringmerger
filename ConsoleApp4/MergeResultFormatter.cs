﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp4
{
    public class MergeResultFormatter
    {
        private readonly uint totalStrings;
        private readonly EqualityFormattingStrategy strategy;
        private List<MergeResult> results;

        private const string EqualitySymbol = "=";

        public enum EqualityFormattingStrategy
        {
            EqualitySymbol,
            StringIds
        }

        public MergeResultFormatter(uint totalStrings, EqualityFormattingStrategy strategy, IEnumerable<MergeResult> results)
        {
            this.totalStrings = totalStrings;
            this.strategy = strategy;
            this.results = results.ToList();
        }

        public string Format()
        {
            var orderedResults = results
                // Order descendingly by the number of occurrences
                .OrderByDescending(res => res.MaxOccurrences)
                // Then we have to put first those characters that 
                // are not contained in all strings
                .ThenByDescending(res => res.ContainedInString.Count != totalStrings)
                // Finally order all chars alphabetically
                // NOTE: We're sorting all groups alphabetically, not just 
                // those where the char appears in all strings. The description
                // doesn't clarify this invariant.
                .ThenBy(res => res.Character);

            return string.Join('/', orderedResults.Select(FormatSingle));
        }

        private string FormatSingle(MergeResult result)
        {
            var sb = new StringBuilder();
            if (result.ContainedInString.Count == totalStrings)
            {
                sb.Append(this.strategy == EqualityFormattingStrategy.EqualitySymbol
                    ? EqualitySymbol
                    : string.Join(',', result.ContainedInString));
            }
            else
            {
                sb.Append(string.Join(',', result.ContainedInString));
            }
            sb.Append(":");
            sb.Append(new String(result.Character, (int)result.MaxOccurrences));
            return sb.ToString();
        }
    }
}