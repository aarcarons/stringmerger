﻿using System;
using System.Collections.Generic;
using StringId = System.Int32;

namespace ConsoleApp4
{
    public class MergeResult : IEquatable<MergeResult>
    {
        public MergeResult(char character)
        {
            this.Character = character;
            this.ContainedInString = new List<int>();
        }

        public char Character { get; }

        public uint MaxOccurrences { get; set; }

        public List<StringId> ContainedInString { get; }

        public override bool Equals(object obj) => Equals(obj as MergeResult);

        public bool Equals(MergeResult other)
        {
            return other != null &&
                   Character == other.Character &&
                   MaxOccurrences == other.MaxOccurrences &&
                   this.ContainedInString.TrueForAll(other.ContainedInString.Contains);
        }

        public override int GetHashCode()
        {
            var hashCode = 2112386933;
            hashCode = hashCode * -1521134295 + Character.GetHashCode();
            hashCode = hashCode * -1521134295 + MaxOccurrences.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<List<int>>.Default.GetHashCode(ContainedInString);
            return hashCode;
        }

        public override string ToString() =>
            $"'{Character}' => {MaxOccurrences} in {string.Join(',', ContainedInString)}";

        public static bool operator ==(MergeResult result1, MergeResult result2) => EqualityComparer<MergeResult>.Default.Equals(result1, result2);

        public static bool operator !=(MergeResult result1, MergeResult result2) => !(result1 == result2);
    }
}