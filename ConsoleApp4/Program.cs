﻿using System;
using System.Collections.Generic;
using StringId = System.Int32;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<StringId, uint[]>();
            for (int i = 1; i < args.Length; i++)
            {
                dict.Add(i, MergerEngine.BuildCharMap(args[i]));
            }
            var results = MergerEngine.MergeMaps(dict);

            var formatter = new MergeResultFormatter(
                2, 
                MergeResultFormatter.EqualityFormattingStrategy.EqualitySymbol,
                results);
            Console.WriteLine(formatter.Format());
        }
    }
}